import json
from bs4 import BeautifulSoup
from selenium import webdriver
from selenium.webdriver.chrome.service import Service as ChromeService
from webdriver_manager.chrome import ChromeDriverManager
import time
import logging

logging.basicConfig(level=logging.DEBUG)

# def fetch_movie_data(url):
#     options = webdriver.ChromeOptions()
#     options.add_argument("--headless")
#     service = ChromeService(ChromeDriverManager().install())
#     driver = webdriver.Chrome(service=service, options=options)
#
#     try:
#         driver.get(url)
#         time.sleep(5)  # Ждём загрузки страницы и прохождения CAPTCHA
#         html = driver.page_source
#         return parse_movie_data_jsonld(html)
#     finally:
#         driver.quit()

import logging

logging.basicConfig(level=logging.DEBUG)

def fetch_movie_data(url):
    # Мок-данные для тестов
    if 'mock' in url:
        data = {
            'title': 'Зеленая миля',
            'description': 'Пол Эджкомб — начальник блока смертников в тюрьме «Холодная гора», каждый из узников которого однажды проходит «зеленую милю» по пути к месту казни. Пол повидал много заключённых и надзирателей за время работы. Однако гигант Джон Коффи, обвинённый в страшном преступлении, стал одним из самых необычных обитателей блока.',
            'actors': ['Том Хэнкс', 'Дэвид Морс', 'Бонни Хант', 'Майкл Кларк Дункан', 'Джеймс Кромуэлл', 'Майкл Джитер', 'Грэм Грин', 'Даг Хатчисон', 'Сэм Рокуэлл', 'Барри Пеппер'],
            'directors': ['Фрэнк Дарабонт'],
            'rating': 9.076,
            'rating_count': 987644,
            'image': 'https://avatars.mds.yandex.net/get-kinopoisk-image/1599028/4057c4b8-8208-4a04-b169-26b0661453e3/600x900'
        }
        logging.debug(f"Извлеченные данные: {data}")
        return data
    else:
        options = webdriver.ChromeOptions()
        options.add_argument("--headless")
        service = ChromeService(ChromeDriverManager().install())
        driver = webdriver.Chrome(service=service, options=options)

        try:
            driver.get(url)
            time.sleep(5)  # Ждём загрузки страницы и прохождения CAPTCHA
            html = driver.page_source
            return parse_movie_data_jsonld(html)
        finally:
            driver.quit()

def parse_movie_data_jsonld(html):
    soup = BeautifulSoup(html, 'html.parser')
    json_ld_script = soup.find('script', type='application/ld+json')
    if json_ld_script:
        try:
            data = json.loads(json_ld_script.string.strip())
            logging.debug(f"JSON Data: {data}")
        except json.JSONDecodeError:
            logging.error("Ошибка при разборе JSON данных")
            return 'Ошибка при разборе JSON данных'
        if data.get('@type') == 'Movie':
            result = {
                'title': data.get('name'),
                'description': data.get('description'),
                'actors': [actor['name'] for actor in data.get('actor', [])],
                'directors': [director['name'] for director in data.get('director', [])],
                'rating': data.get('aggregateRating', {}).get('ratingValue'),
                'rating_count': data.get('aggregateRating', {}).get('ratingCount'),
                'image': data.get('image')
            }
            logging.debug(f"Извлеченные данные: {result}")
            return result
    logging.error("JSON-LD скрипт не найден или данные невалидны")
    return 'Данные о фильме не найдены или страница изменена'
