import json
from flask import Flask, request, jsonify, Response
from parser import fetch_movie_data
import logging

app = Flask(__name__)

@app.route('/')
def home():
    return "Добро пожаловать в API Fetch Movie!"

@app.route('/fetch-movie')
def fetch_movie():
    movie_url = request.args.get('url')
    logging.debug(f"Requested URL: {movie_url}")
    if movie_url:
        data = fetch_movie_data(movie_url)
        return Response(json.dumps(data, ensure_ascii=False), mimetype='application/json;charset=utf-8')
    else:
        return jsonify({"error": "No URL provided"}), 400

if __name__ == '__main__':
    app.run(debug=True)
