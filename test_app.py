import pytest
from app import app

@pytest.fixture
def client():
    app.config['TESTING'] = True
    with app.test_client() as client:
        yield client

def test_fetch_movie(client):
    response = client.get('/fetch-movie?url=mock://test')
    assert response.status_code == 200
    data = response.get_json()
    assert data['title'] == 'Зеленая миля'
    assert 'Пол Эджкомб' in data['description']

def test_search_movie(client):
    response = client.get('/fetch-movie?url=mock://test')
    assert response.status_code == 200
    # Так как у нас нет фактического поискового эндпоинта, мы просто проверяем сохранение данных
    data = response.get_json()
    assert data['title'] == 'Зеленая миля'