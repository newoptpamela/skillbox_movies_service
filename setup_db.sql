CREATE TABLE movies (
    id SERIAL PRIMARY KEY,
    title VARCHAR(255),
    description TEXT,
    actors TEXT,
    directors TEXT,
    release_date DATE,
    country VARCHAR(255),
    genre VARCHAR(255),
    rating FLOAT,
    rating_count INTEGER,
    image VARCHAR(255)
);